# Squeryl example

This is a sample project on how to use relational databases in Scala.

I’ve decided to use:

* [Squeryl](http://squeryl.org/index.html) 0.9.5-2 as a DSL for accessing the DB
* [Flyway](http://flywaydb.org/) 2.0 for database migrations based in plain old SQL
* [PostgreSQL](http://www.postgresql.org/) 9.2.1 as the relational DB
* [C3P0](http://www.mchange.com/projects/c3p0/) 0.9.1.2 as the database pool, which I need just to show how to use such a pool with Squeryl
* Scala 2.9.2, since it is the latest stable version (at the time of writing)
* [Scalatest](http://www.scalatest.org/) 1.8 for unit and integration testing

## Why Squeryl

Squeryl is a framework for relational database access from Scala. Alhough they define it as “Scala ORM”, it plays fairly well with tuple-like classes with no methods at all. Furthermore, it offers a statically typed DSL that should check your SQL at compile time.

So, its main duties are:

* Define how to create Scala classes that represent the DB schema you have
* Map your query results to the Scala classes used to define the schema (+tuples and whatever is needed for more complex queries)
* Map your Scala instances to insert and similar operations without requiring you to build the needed SQL
* Define a statically typed Scala DSL for arbitrary SQL queries

## Why Flyway

I want migrations. If you don’t know what they are or why I (and you) need them, check the excellent [justification by Flyway guys](http://flywaydb.org/getstarted/whyDatabaseMigrations.html).

And... to keep things simple and to minimize dependencies to complex stuff, I wanted to write migrations in plain old SQL. Sure, I won’t be able to switch DB vendors easy... But, know what, I personally changed the DB vendor of a project just once or twice... and I needed to change code, run tests, etc. So, I can live with having to rewrite my SQL if that happens.

That’s the reason I discarded [Scala-Migrations](http://code.google.com/p/scala-migrations/) for different reasons. 

