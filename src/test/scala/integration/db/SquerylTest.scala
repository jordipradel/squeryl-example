package integration.db

import org.scalatest.{FlatSpec, BeforeAndAfterAll}
import org.squeryl.PrimitiveTypeMode._
import sample.db.{LibrarySchema, Book, Author}
import sample.db.LibrarySchema._
import sample.Application
import org.squeryl.Query


class SquerylTest extends FlatSpec with BeforeAndAfterAll {

  override def convertToEqualizer(left: Any) = new Equalizer(left)

  /** Method to replace assert(value === expected) since === is used by Squeryl */
  def assertEquals[T](value:T, expected:T) {
    assert(convertToEqualizer(value) === expected)
  }

  behavior of "Squeryl"

  override protected def beforeAll() {
    Application.migrations.clean()
    Application.init()
    transaction {
      //LibrarySchema.printDdl
      val odersky = authors.insert(new Author(firstName="Martin",lastName="Odersky"))
      books.insert(new Book(title="Programming in Scala", authorId=odersky.id))
    }

  }

  it should "execute simple queries" in {
    transaction {
      val q:Query[String] = from(authors)(a => where(a.firstName === "Martin") select(a.firstName))
      val name = q.head
      assertEquals(name,"Martin")
    }
  }

}
