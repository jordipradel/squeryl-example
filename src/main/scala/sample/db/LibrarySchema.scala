package sample.db

import org.squeryl.{Schema => SquerylSchema, KeyedEntity}
import org.squeryl.PrimitiveTypeMode._

case class Author(id: Long = 0,
              firstName: String,
              lastName: String,
              email: Option[String] = None) extends KeyedEntity[Long]{
  def this() = this(0,"","",Some(""))
}

case class Book( id: Long = 0,
            title: String,
            authorId: Long) extends KeyedEntity[Long]{

  def this() = this(0,"",0)
}

object LibrarySchema extends SquerylSchema{

  val authors = table[Author]

  val books = table[Book]

  on(authors)(s => declare(
    s.email      is(unique,indexed("idxEmailAddresses")),
    s.firstName  is(indexed("idxFirstName")),
    s.lastName   is(indexed("idxLastName")),
    columns(s.firstName, s.lastName) are(indexed("idxName"))
  ))
}