package sample

import org.squeryl.adapters.PostgreSqlAdapter
import org.squeryl.{Session, SessionFactory}
import com.mchange.v2.c3p0.ComboPooledDataSource
import com.googlecode.flyway.core.Flyway

object Application {

  val databaseUrl = "jdbc:postgresql://localhost/test"

  val databaseUsername = "test"

  val databasePassword = "test"

  val dataSource = {
    val ds = new ComboPooledDataSource
    ds.setDriverClass("org.postgresql.Driver")
    ds.setJdbcUrl(databaseUrl)
    ds.setUser(databaseUsername)
    ds.setPassword(databasePassword)
    ds.setMaxPoolSize(5)
    ds
  }

  val migrations = {
    val res = new Flyway()
    res.setDataSource(dataSource)
    res
  }

  def init() {
    SessionFactory.concreteFactory = Some(() => Session.create(dataSource.getConnection, new PostgreSqlAdapter))
    migrations.migrate()
  }

}
