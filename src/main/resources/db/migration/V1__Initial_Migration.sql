-- table declarations :
create table "Author" (
    "email" varchar(128),
    "lastName" varchar(128) not null,
    "firstName" varchar(128) not null,
    "id" bigint primary key not null
  );
create sequence "s_Author_id";
-- indexes on Author
create unique index "idxEmailAddresses" on "Author" ("email");
create index "idxLastName" on "Author" ("lastName");
create index "idxFirstName" on "Author" ("firstName");
create table "Book" (
    "id" bigint primary key not null,
    "authorId" bigint not null,
    "title" varchar(128) not null
  );
create sequence "s_Book_id";
-- column group indexes :
create index "idxName" on "Author" ("firstName","lastName");